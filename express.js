const express = require('express')
const app = express()
const port = 3000

app.get('/', function(req, res) {
	res.send('Hello! You can go <a href="/parameter">here</a> and give me a querystring');
});

app.get('/parameter', function(req, res) {
	let qs = req.query;
	let responseText = "";
	for (var key in qs) {
		if (! responseText){
			responseText = '"' + key + ' : '  + qs[key] + '"';
		}else{
			responseText = responseText + (', "' + key + ' : ' + qs[key] + '"');
		}
	}
	if (responseText === "") {
		responseText = "Please give me a querystring";
	}
	res.send(responseText);
});

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))